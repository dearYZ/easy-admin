package com.mars.module.tool.entity;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.io.Serializable;
import cn.afterturn.easypoi.excel.annotation.Excel;
import java.util.Date;


/**
 * 操作日志记录对象 sys_oper_log
 *
 * @author mars
 * @date 2023-11-17
 */

@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@ApiModel(value = "操作日志记录对象")
@Accessors(chain = true)
@TableName("sys_oper_log")
public class SysOperLog {



    /**
     * 日志主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @ApiModelProperty(value = "id")
    private Long id;

    /**
     * 模块标题
     */
    @Excel(name = "模块标题")
    @ApiModelProperty(value = "模块标题")
    private String title;

    /**
     * 业务类型（0其它 1新增 2修改 3删除）
     */
    @ApiModelProperty(value = "业务类型（0其它 1新增 2修改 3删除）")
    private Integer businessType;

    /**
     * 方法名称
     */
    @Excel(name = "方法名称")
    @ApiModelProperty(value = "方法名称")
    private String method;

    /**
     * 请求方式
     */
    @Excel(name = "请求方式")
    @ApiModelProperty(value = "请求方式")
    private String requestMethod;

    /**
     * 操作类别（0其它 1后台用户 2手机端用户）
     */
    @ApiModelProperty(value = "操作类别（0其它 1后台用户 2手机端用户）")
    private Integer operatorType;

    /**
     * 操作人员
     */
    @Excel(name = "操作人员")
    @ApiModelProperty(value = "操作人员")
    private String operName;

    /**
     * 部门名称
     */
    @Excel(name = "部门名称")
    @ApiModelProperty(value = "部门名称")
    private String deptName;

    /**
     * 请求URL
     */
    @Excel(name = "请求URL")
    @ApiModelProperty(value = "请求URL")
    private String operUrl;

    /**
     * 主机地址
     */
    @Excel(name = "主机地址")
    @ApiModelProperty(value = "主机地址")
    private String operIp;

    /**
     * 操作地点
     */
    @Excel(name = "操作地点")
    @ApiModelProperty(value = "操作地点")
    private String operLocation;

    /**
     * 请求参数
     */
    @Excel(name = "请求参数")
    @ApiModelProperty(value = "请求参数")
    private String operParam;

    /**
     * 返回参数
     */
    @Excel(name = "返回参数")
    @ApiModelProperty(value = "返回参数")
    private String jsonResult;

    /**
     * 操作状态（0正常 1异常）
     */
    @ApiModelProperty(value = "操作状态（0正常 1异常）")
    private Integer status;

    /**
     * 错误消息
     */
    @Excel(name = "错误消息")
    @ApiModelProperty(value = "错误消息")
    private String errorMsg;

    /**
     * 操作时间
     */
    @Excel(name = "操作时间" , width = 30, databaseFormat = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "操作时间")
    private Date operTime;
}
