package com.mars.module.tool.request;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.mars.common.request.PageRequest;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-11-08 09:44:42
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "已导入表请求参数")
public class GenTableRequest extends PageRequest {

    /**
     * 编号
     */
    @ApiModelProperty(value = "编号")
    private Long tableId;

    /**
     * 表名称
     */
    @ApiModelProperty(value = "表名称")
    private String tableName;

    /**
     * 表描述
     */
    @ApiModelProperty(value = "表描述")
    private String tableComment;

}
