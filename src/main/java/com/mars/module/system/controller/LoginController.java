package com.mars.module.system.controller;

import com.mars.common.request.sys.LoginRequest;
import com.mars.common.response.sys.LoginResponse;
import com.mars.common.response.sys.UserInfoResponse;
import com.mars.common.result.R;
import com.mars.common.request.sys.DataUpdateRequest;
import com.mars.common.util.TokenUtils;
import com.mars.module.base.BaseController;
import com.mars.module.system.service.ISysUserService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


/**
 * 登录控制器
 *
 * @author 源码字节-程序员Mars
 */
@RestController
@Api(tags = "登录控制器")
public class LoginController extends BaseController {

    @Resource
    private ISysUserService sysUserService;

    public LoginController(TokenUtils tokenUtils) {
        super(tokenUtils);
    }

    /**
     * 登录
     *
     * @param loginRequest 请求参数
     * @param request      request
     * @param response     response
     * @return R
     */
    @PostMapping("/login")
    @ApiOperation(value = "登录")
    public R<LoginResponse> login(@Validated @RequestBody LoginRequest loginRequest, HttpServletRequest request, HttpServletResponse response) {
        return R.success(sysUserService.login(loginRequest, request, response));
    }

    /**
     * 退出
     *
     * @param request request
     * @return R
     */
    @PostMapping("/logout")
    @ApiOperation(value = "退出")
    public R<Void> logout(HttpServletRequest request) {
        sysUserService.logout(request);
        return R.success();
    }

    /**
     * 修改个人资源
     *
     * @param updateDto updateDto
     * @return R
     */
    @PostMapping("/updateInfo")
    @ApiOperation(value = "修改个人资料")
    public R<Void> dataUpdate(@Validated @RequestBody DataUpdateRequest updateDto) {
        sysUserService.dataUpdate(updateDto);
        return R.success();
    }

    /**
     * 获取用户信息
     *
     * @param request  请求参数
     * @param response response
     * @return R
     */
    @ApiOperation(value = "获取用户信息")
    @GetMapping("/getInfo")
    public R<UserInfoResponse> getInfo(HttpServletRequest request, HttpServletResponse response) {
        return R.success(sysUserService.getInfo(getUserId(), request, response));
    }

    /**
     * 获取未读消息数量
     *
     * @return R
     */
    @GetMapping("message/unRead")
    @ApiOperation(value = "获取未读消息数量")
    public R<Integer> getUnReadNum() {
        return R.success(sysUserService.getUnReadNum(getUserId()));
    }

}
