package com.mars.framework.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author 程序员Mars
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface RateLimiter {
    /**
     * 过期时间
     *
     * @return 时间(单位毫秒)
     */
    int expireTime() default 1000;

    /**
     * 默认的key
     *
     * @return key
     */
    String key() default "";

}
