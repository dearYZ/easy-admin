package com.mars.common.request.sys;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * 登录DTO
 *
 * @author 源码字节-程序员Mars
 */
@Data
@ApiModel(value = "登录请求参数")
public class LoginRequest {

    @NotEmpty(message = "请输入用户名")
    @ApiModelProperty(value = "用户名")
    private String userName;

    @NotEmpty(message = "请输入密码")
    @ApiModelProperty(value = "密码")
    private String password;

    @NotEmpty(message = "请输入验证码")
    @ApiModelProperty(value = "验证码")
    private String code;

    @NotEmpty(message = "请输入用户名")
    @ApiModelProperty(value = "uuid")
    private String uuid;


}
